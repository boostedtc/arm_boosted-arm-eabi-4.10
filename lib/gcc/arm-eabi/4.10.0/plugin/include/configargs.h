/* Generated automatically. */
static const char configuration_arguments[] = ".././../gcc/gcc-Boosted/configure --prefix=/tmp/arm-eabi-4.10 --target=arm-eabi --host=x86_64-linux-gnu --build=x86_64-linux-gnu --with-gnu-as --with-gnu-ld --enable-languages=c,c++ --with-pkgversion=Boosted-Toolchain --with-gmp=/home/hieu/toolchains/google/build/temp-install --with-mpfr=/home/hieu/toolchains/google/build/temp-install --with-mpc=/home/hieu/toolchains/google/build/temp-install --without-ppl --without-cloog --without-isl --disable-libssp --enable-threads --disable-nls --disable-libmudflap --disable-libgomp --disable-libstdc__-v3 --disable-sjlj-exceptions --disable-shared --disable-tls --disable-libitm --with-float=soft --with-fpu=neon --with-arch=armv7-a --enable-target-optspace --with-abi=aapcs --prefix=/tmp/arm-eabi-4.10 --with-gcc-version=Boosted --with-binutils-version=current --with-gmp-version=current --with-mpfr-version=current --with-mpc-version=current --with-gdb-version=current --with-sysroot=/opt/android-ndk/platforms/android-19/arch-arm --with-cloog-version=current --program-transform-name='s&^&arm-eabi-&'";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "abi", "aapcs" }, { "arch", "armv7-a" }, { "float", "soft" }, { "fpu", "neon" } };
